class CreateLights < ActiveRecord::Migration
  def change
    create_table :lights do |t|
      t.string :name
      t.integer :light_id
      t.integer :hub_id

      t.timestamps null: false
    end
  end
end
