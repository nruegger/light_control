class HubsController < ApplicationController
  def index
  end
  
  def show
    @hub = Hubs.find(params[:id])
    @lights = Lights.where(hub_id: params[:id])
    @light = ""
    if @lights.length == 0
      #will show that no lights have been added yet
      @light = "none found, please add lights"
    else
      @lights.each do |l|
        #lights found, creating string for display
        @light = @light + ", " + l.name
      end
    end
  end
  
  def new
  end
  
  def edit
  end
  
  def create
    #these setting can be used to define hub IP or a custom username
    #config.hue_ip = '123.456.789.012'
    #config.uuid = '0123456789abdcef0123456789abcdef' 
    
    #initializes the username, must press button afterward
    Huey::Request.register
    sleep(30)#wait 30 seconds for hub button press
    registery = Huey::Request.register
    if defined? registery[0]
      #username was whitelisted
      @hub = Hub.new
      @hub.name = hub_params[:name]
      @hub.username = registery[0]["success"]["username"]
      @hub.save
      redirect_to @hub
    else
      #button not pressed in time
      redirect_to :action=>"new"
    end
  end
    
  def update
  end
  
  def destroy
  end
  
  private
    def hub_params
      params.require(:widget).permit(:name)
    end
end
