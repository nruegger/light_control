class LightsController < ApplicationController
  def index
    @lights = Lights.all
  end
  
  def show
    @light = Lights.find(params[:id])
    @details = Huey::Bulb.find(@light.light_id)
  end

  def new
  end
  
  def edit
  end
  
  def create
    #just auto adds bulbs when going to create action for now
    @light_list = Huey::Bulb.find_all("")
    @light_list.each do |l|
      @light = Light.new
      @light.name = l.name
      @light.light_id = l.id
      @light.hub_id = 1 #default for now, will need to pass hub id eventually
      @light.save!
    end
  end
  
  def update
  end
  
  def destroy
  end
  
end
